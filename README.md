# MSPR_BI_DataAnalyse

Ce projet vient en complèment du l'UE Data Analyse & Big Data, de la 4ème année de l'EPSI. 

Nous avons choisi un dataset portant sur la musique, comprenant 4 fichiers .csv (Genres, Albums, Critics, Artists).

Nous utilisons le logiciel [QlickSense](https://www.qlik.com/fr-fr/products/qlik-sense) (version en ligne) pour générer des rapports de données, dans le but de faire une analyse.

Les fichiers Genres, Critics et Artists deviennent des Dimentions. 
Albums devient une table de Fait.

# Pour installer le projet :

Sur Qlik Sense sélectionner "Nouvelle application" puis "Télécharger une application" et choisir le .qvf

Pour l'intégration des données dans l'application voir le fichier .pdf
